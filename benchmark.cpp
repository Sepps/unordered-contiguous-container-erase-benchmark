#include <benchmark/benchmark.h>
#include <random>
#include <array>
#include <algorithm>

using elem = int;
using vec = std::vector<elem>;
using vec_iterator = typename vec::iterator; 

std::random_device rd;
std::uniform_int_distribution<elem> dist(0, 100);

auto Rand()
{
  return dist(rd);
}

template <size_t N>
auto Gen()
{
  vec output;
  output.resize(N);
  for (size_t i = 0; i < N; ++i) output[i] = Rand();
  return output;
}
//-----------------------------------------------------------------------------------------------------
[[gnu::always_inline]] inline static void MoveAndPop(auto & output, size_t index) noexcept
{
  output[index] = std::move(output.back());
  output.pop_back();
}
template <size_t N>
static void MoveAndPop(benchmark::State& state)
{
  for (auto _ : state)
  {
    auto match = Rand();
    auto output = Gen<N>();
    benchmark::DoNotOptimize(output);

    for (size_t i = 0; i < output.size(); )
    {
      if (output[i] == match)
      {
        MoveAndPop(output, i);
      }
      else
      {
        ++i;
      }
    }
  }
}
//-----------------------------------------------------------------------------------------------------
[[gnu::always_inline]] inline static void MoveAndPopBatch(auto & output, size_t index, size_t & len) noexcept
{
  output[index] = std::move(output.back());
  --len;
}
template <size_t N>
static void MoveAndPopBatch(benchmark::State& state)
{
  for (auto _ : state)
  {
    auto match = Rand();
    auto output = Gen<N>();
    auto len = output.size();
    benchmark::DoNotOptimize(output);

    for (size_t i = 0; i < len; )
    {
      if (output[i] == match)
      {
        MoveAndPopBatch(output, i, len);
      }
      else
      {
        ++i;
      }
    }

    for (; len < output.size();++len)
      output.pop_back();
  }
}
//-----------------------------------------------------------------------------------------------------
template <size_t N>
static void MoveAndPopBatchErase(benchmark::State& state)
{
  for (auto _ : state)
  {
    auto match = Rand();
    auto output = Gen<N>();
    auto len = output.size();
    benchmark::DoNotOptimize(output);

    for (size_t i = 0; i < len; )
    {
      if (output[i] == match)
      {
        MoveAndPopBatch(output, i, len);
      }
      else
      {
        ++i;
      }
    }

    output.erase(output.begin() + len, output.end());
  }
}
//-----------------------------------------------------------------------------------------------------
[[gnu::always_inline]] inline static void MoveAndDec(auto & begin, auto & end) noexcept
{
  *begin = std::move(*end);
  --end;
}
template <size_t N>
static void MoveIteratorThenBatchErase(benchmark::State& state)
{
  for (auto _ : state)
  {
    auto match = Rand();
    auto output = Gen<N>();
    benchmark::DoNotOptimize(output);
    auto begin = output.begin();
    auto end = output.end();
    while (begin != end)
    {
      if (*begin == match)
      {
        MoveAndDec(begin, end);
      }
      else
      {
        ++begin;
      }
    }
    output.erase(end, output.end());
  }
}
//-----------------------------------------------------------------------------------------------------
[[gnu::always_inline]] inline static void MoveAndInc(auto & begin, auto & end) noexcept
{
  *end = std::move(*begin);
  ++begin;
}
template <size_t N>
static void MoveIteratorThenBatchEraseReverse(benchmark::State& state)
{
  for (auto _ : state)
  {
    auto match = Rand();
    auto output = Gen<N>();
    benchmark::DoNotOptimize(output);
    auto begin = output.begin();
    auto end = output.end();
    if(begin != end)
    {
      // Make sure first deref is in range.
      --end;
    }
    // a general algorithm would abort if begin == end, but it never does here...
    while (begin != end)
    {
      auto & elem = *end;
      if (elem == match)
      {
        MoveAndInc(begin, end);
      }
      else
      {
        --end;
      }
    }
    output.erase(output.begin(), begin);
  }
}

static void MoveAndPop_16                                 (benchmark::State& state) { MoveAndPop<16>(state);                               }
static void MoveAndPop_512                                (benchmark::State& state) { MoveAndPop<512>(state);                              }
static void MoveAndPop_65536                            (benchmark::State& state) { MoveAndPop<65536>(state);                          }
static void MoveAndPopBatch_16                            (benchmark::State& state) { MoveAndPopBatch<16>(state);                          }
static void MoveAndPopBatch_512                           (benchmark::State& state) { MoveAndPopBatch<512>(state);                         }
static void MoveAndPopBatch_65536                       (benchmark::State& state) { MoveAndPopBatch<65536>(state);                     }
static void MoveAndPopBatchErase_16                       (benchmark::State& state) { MoveAndPopBatchErase<16>(state);                     }
static void MoveAndPopBatchErase_512                      (benchmark::State& state) { MoveAndPopBatchErase<512>(state);                    }
static void MoveAndPopBatchErase_65536                  (benchmark::State& state) { MoveAndPopBatchErase<65536>(state);                }
static void MoveIteratorThenBatchErase_16                 (benchmark::State& state) { MoveIteratorThenBatchErase<16>(state);               }
static void MoveIteratorThenBatchErase_512                (benchmark::State& state) { MoveIteratorThenBatchErase<512>(state);              }
static void MoveIteratorThenBatchErase_65536            (benchmark::State& state) { MoveIteratorThenBatchErase<65536>(state);          }
static void MoveIteratorThenBatchEraseReverse_16          (benchmark::State& state) { MoveIteratorThenBatchEraseReverse<16>(state);        }
static void MoveIteratorThenBatchEraseReverse_512         (benchmark::State& state) { MoveIteratorThenBatchEraseReverse<512>(state);       }
static void MoveIteratorThenBatchEraseReverse_65536     (benchmark::State& state) { MoveIteratorThenBatchEraseReverse<65536>(state);   }

BENCHMARK(MoveAndPop_16);
BENCHMARK(MoveAndPop_512);
BENCHMARK(MoveAndPop_65536);
BENCHMARK(MoveAndPopBatch_16);
BENCHMARK(MoveAndPopBatch_512);
BENCHMARK(MoveAndPopBatch_65536);
BENCHMARK(MoveAndPopBatchErase_16);
BENCHMARK(MoveAndPopBatchErase_512);
BENCHMARK(MoveAndPopBatchErase_65536);
BENCHMARK(MoveIteratorThenBatchErase_16);
BENCHMARK(MoveIteratorThenBatchErase_512);
BENCHMARK(MoveIteratorThenBatchErase_65536);
BENCHMARK(MoveIteratorThenBatchEraseReverse_16);
BENCHMARK(MoveIteratorThenBatchEraseReverse_512);
BENCHMARK(MoveIteratorThenBatchEraseReverse_65536);

BENCHMARK_MAIN();